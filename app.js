
/**
 * Module dependencies.
 */

var express = require('express');
var flash = require('connect-flash');
var http = require('http');
var passport = require('passport');
var path = require('path');

var routes = require('./routes');
var user = require('./routes/user');
var cards = require('./routes/cards');
var poll = require('./routes/poll');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.cookieParser());
app.use(express.session({ secret: 'Jaffa Cakes' }));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use(app.router);

// 404
app.use(
    function(req, res){
        res.status(404).render(
            '404',
            {
                user: req.user
            }
        );
    }
);

// Development only
app.use(express.errorHandler());
app.locals.pretty = true;

// Index
app.get('/', routes.index);

// User
app.post('/login', user.passportlogin);
app.get('/login', user.login);
app.get('/logout', user.logout);
app.all('/register', user.register);
app.all('/reset', user.reset);
app.all('/user', user.user);

// Cards
app.get('/cards', cards.index);
app.post('/cards/set', cards.create);
app.all('/cards/set/:set_id', cards.set);
app.post('/cards/set/:set_id/options', cards.options);
app.post('/cards/set/:set_id/delete', cards.delete);
app.post('/cards/set/:set_id/newcard', cards.newcard);
app.post('/cards/set/:set_id/deletecard', cards.deletecard);
app.post('/cards/set/:set_id/upload', cards.uploadcards);

// Poll
app.get('/poll', poll.index);
app.get('/poll/:poll_id', poll.detail);
app.get('/poll/:poll_id/results', poll.results);
app.get('/poll/:poll_id/vote', poll.vote);
app.post('/poll/:poll_id/vote', poll.vote);

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
