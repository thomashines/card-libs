function deletecard(row, white, id) {
    $.post('/cards/set/'+$('#cardset').val()+'/deletecard/',
    {
        white: white,
        id: id
    },
    function(data, status){
        $(row).closest('tr').remove();
    });
    return false;
}
$(document).ready(function(){
    $('#home a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $('#black a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $('#white a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $('#blackadd').click(function(){
        $.post('/cards/set/'+$('#cardset').val()+'/newcard/',
        {
            white: 0,
            prompt: $('#blackprompt').val()
        },
        function(data, status){
            $('#blackprompt').val('');
            $('#blacktable > tbody:first').append('<tr><td>'+data.prompt+'</td><td>'+data.blanks+'</td><td><a onclick="deletecard(this, 0, \''+data.id+'\')" role="button" class="btn btn-danger btn-xs">Delete</a></td></tr>');
        });
        return false;
    });
    $('#whiteadd').click(function(){
        $.post('/cards/set/'+$('#cardset').val()+'/newcard/',
        {
            white: 1,
            text: $('#whitetext').val()
        },
        function(data, status){
            $('#whitetext').val('');
            $('#whitetable > tbody:first').append('<tr><td>'+data.text+'</td><td><a onclick="deletecard(this, 1, \''+data.id+'\')" role="button" class="btn btn-danger btn-xs">Delete</a></td></tr>');
        });
        return false;
    });
});

