var nodemailer = require('nodemailer');

exports.send = function(to, subject, message) {
    var smtpTransport = nodemailer.createTransport(
        'SMTP',
        {
            service: 'Gmail',
            auth:
                {
                    user: 'pitdrone@gmail.com',
                    pass: 'NoPassword'
                }
        }
    );

    var mailOptions = {
        from: "Card Libs <pitdrone@gmail.com>",
        to: to,
        subject: 'Card Libs | ' + subject,
        html: message
    }

    smtpTransport.sendMail(
        mailOptions,
        function(error, response) {
            if(error) {
                console.log(error);
            } else {
                console.log("Message sent: " + response.message);
            }
            smtpTransport.close();
        }
    );
}
