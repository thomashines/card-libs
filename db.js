var mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost/cardlibs');
mongoose.connect('mongodb://card:libs@troup.mongohq.com:10025/cardlibs');

exports.conn = mongoose.connection;

exports.conn.on(
    'error',
    console.error.bind(
        console,
        'Database connection error:'
    )
);

exports.conn.once(
    'open',
    function callback() {
        console.log("Cards database connected.")
    }
);