var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Poll
exports.pollSchema = new Schema({
    question: {type: String, required: true},
    pubDate: {type: Date, required: true},
    choices: [{
        choiceText: {type: String, required: true},
        votes: {type: Number, required: true, default: 0}
    }]
}, {
    collection: 'poll'
});

exports.pollSchema.methods.wasPublishedRecently = function () {
    var now = new Date()
    var delta = Math.abs(now.getTime() - this.pubDate.getTime())
    // If the difference is less than
    // the number of milliseconds in a day
    return delta <= (60 * 60 * 24 * 1000)
}

mongoose.model('Poll', exports.pollSchema);