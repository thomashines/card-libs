// Card
exports.card = {
    text: {type: String, required: true},
    blanks: {type: Number, required: true, default: 0}
}