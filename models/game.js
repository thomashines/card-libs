var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var card = require('./card');

// Game
exports.gameSchema = new Schema({
    name: {type: String, required: true},
    owner: Schema.ObjectId,
    pub: {type: Boolean, required: true, default: false},
    date: {type: Date, default: Date.now},
    log: {type: String, required: true},
    phase: {type: Number, required: true, default: 0}
    players: [{
        user: Schema.ObjectId,
        cards: [{
            text: {type: String, required: true}
        }],
        played: {
            text: {type: String, required: true}
        },
        score: {type: Number, required: true, default: 0}
    }]
    whitepool: [{
        text: {type: String, required: true}
    }],
    blackpool: [{
        prompt: {type: String, required: true},
        blanks: {type: Number, required: true, default: 0}
    }],
    black: {
        prompt: {type: String, required: true},
        blanks: {type: Number, required: true, default: 0}
    },
    winner: Schema.ObjectId,
    czar: Schema.ObjectId
}, {
    collection: 'game'
});

mongoose.model('Game', exports.gameSchema);