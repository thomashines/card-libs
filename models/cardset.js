var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var card = require('./card');

// Card Set
exports.cardSetSchema = new Schema({
    name: {type: String, required: true},
    owner: {type: Schema.ObjectId, required: true},
    pub: {type: Boolean, required: true, default: false},
    blackcards: [{
        prompt: {type: String, required: true},
        blanks: {type: Number, required: true, default: 0}
    }],
    whitecards: [{
        text: {type: String, required: true}
    }]
}, {
    collection: 'cardset'
});

mongoose.model('CardSet', exports.cardSetSchema);