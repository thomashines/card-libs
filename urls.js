var routes = require('./routes');
var user = require('./routes/user');
var polls = require('./routes/polls');

// app.get('/', routes.index);
// app.post('/login', user.login);
// app.get('/polls', polls.pollIndex);
// app.get('/polls/:poll_id', polls.detail);
// app.get('/polls/:poll_id/results', polls.detail);
// app.get('/polls/:poll_id/vote', polls.detail);
// app.post('/polls/:poll_id/vote', polls.detail);

module.exports = [
    { name : "index", pattern : "/", view : routes.index },
    { name : "login", pattern : "/login", view : user.login },
    { name : "pollIndex", pattern : "/polls", view : polls.pollIndex },
    { name : "pollIndex", pattern : "/polls/:poll_id", view : polls.details },
    { name : "pollIndex", pattern : "/polls/:poll_id/results", view : polls.results },
    { name : "pollIndex", pattern : "/polls/:poll_id/vote", view : polls.vote }
]