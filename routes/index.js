
/*
 * GET home page.
 */

exports.index = function(req, res){
    res.render(
        'index',
        {
            user: req.user,
            page: 'Home',
            danger: req.flash('danger'),
            warning: req.flash('warning'),
            success: req.flash('success')
        }
    );
};