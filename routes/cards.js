/*
 * User methods
 */

var formidable = require('formidable');

var db = require('../db');
var userSchemas = require('../models/user');
var User = db.conn.model("User", userSchemas.userSchema);
var cardSetSchemas = require('../models/cardset');
var CardSet = db.conn.model("CardSet", cardSetSchemas.cardSetSchema);

exports.index = function(req, res){
    if(!req.user) return res.redirect('/login/');
    CardSet.find({owner: req.user._id}).exec(function(err, cardsets) {
        if(err) {
            console.log(err);
        } else {
            res.render(
            'cards/index',
            {
                user: req.user,
                page: 'Cards',
                danger: req.flash('danger'),
                warning: req.flash('warning'),
                success: req.flash('success'),
                cardsets: cardsets
            }
        );
        }
    });
};

exports.create = function(req, res){
    if(!req.user) return res.redirect('/login/');
    var cardset = new CardSet({
        name: req.body.name,
        owner: req.user._id
    });
    cardset.save(function (err) {
        if(err) {
            console.log('Error creating card set.');
            req.flash('danger', 'Error creating card set.');
            res.redirect('/cards/');
        } else {
            console.log('Created card set.');
            req.flash('success', 'Created card set.');
            res.redirect('/cards/set/'+cardset._id);
        }
    });
};

exports.options = function(req, res){
    if(!req.user) return res.redirect('/login/');
    CardSet.findById(req.params.set_id, function(err, cardset) {
        if(err) {
            console.log(err);
        } else {
            User.findById(cardset.owner, function(err, owner) {
                if(err) {
                    console.log(err);
                } else if(cardset.owner.equals(req.user._id)) {
                    cardset.pub = (req.body.pub == 'on');
                    cardset.save(function (err) {
                        if (err) {
                            console.log('Failed change card set.');
                        } else {
                            req.flash('success', 'Changed card set settings.');
                            res.redirect('/cards/set/'+cardset._id);
                        }
                    });
                }
            })
        }
    });
};

exports.delete = function(req, res){
    if(!req.user) return res.redirect('/login/');
    if(req.params.set_id != req.body.id) {
        req.flash('danger', 'IDs do not match.');
        return res.redirect('/cards/set/'+cardset._id);
    }
    CardSet.findById(req.params.set_id, function(err, cardset) {
        if(err) {
            console.log(err);
        } else {
            User.findById(cardset.owner, function(err, owner) {
                if(err) {
                    console.log(err);
                } else if(cardset.owner.equals(req.user._id)) {
                    cardset.remove();
                    req.flash('success', 'Deleted card set.');
                    res.redirect('/cards/');
                }
            })
        }
    });
};

exports.set = function(req, res){
    if(!req.user) return res.redirect('/login/');
    CardSet.findById(req.params.set_id, function(err, cardset) {
        if(err) {
            console.log(err);
        } else {
            User.findById(cardset.owner, function(err, owner) {
                if(err) {
                    console.log(err);
                } else if(cardset.owner.equals(req.user._id)) {
                    console.log('My card set.');
                    res.render(
                        'cards/edit',
                        {
                            user: req.user,
                            page: 'Cards',
                            danger: req.flash('danger'),
                            warning: req.flash('warning'),
                            success: req.flash('success'),
                            cardset: cardset,
                            owner: owner
                        }
                    );
                } else {
                    console.log('Someone else\'s card set.');
                    res.render(
                        'cards/view',
                        {
                            user: req.user,
                            page: 'Cards',
                            danger: req.flash('danger'),
                            warning: req.flash('warning'),
                            success: req.flash('success'),
                            cardset: cardset,
                            owner: owner
                        }
                    );
                }
            })
        }
    });
};

exports.newcard = function(req, res){
    if(!req.user) return;
    CardSet.findById(req.params.set_id, function(err, cardset) {
        if(err) {
            console.log(err);
        } else {
            User.findById(cardset.owner, function(err, owner) {
                if(err) {
                    console.log(err);
                } else if(cardset.owner.equals(req.user._id)) {
                    var response = {success: false};
                    if(req.body.white == '0') {
                        var blanks = (req.body.prompt.match(/_/g)||[]).length;
                        if(blanks == 0) {
                            blanks = 1;
                        }
                        cardset.blackcards.push({
                            prompt: req.body.prompt,
                            blanks: blanks
                        });
                        response = {prompt: req.body.prompt, blanks: blanks, id: cardset.blackcards[cardset.blackcards.length-1]._id};
                    } else {
                        cardset.whitecards.push({
                            text: req.body.text
                        });
                        response = {text: req.body.text, id: cardset.whitecards[cardset.whitecards.length-1]._id};
                    }
                    cardset.save(function (err) {
                        if (err) {
                            console.log('Failed to add card.');
                        } else {
                            res.writeHead(200, { 'Content-Type': 'application/json' });
                            res.write(JSON.stringify(response));
                            res.end();
                        }
                    });
                }
            })
        }
    });
};

exports.deletecard = function(req, res){
    if(!req.user) return;
    CardSet.findById(req.params.set_id, function(err, cardset) {
        if(err) {
            console.log(err);
        } else {
            User.findById(cardset.owner, function(err, owner) {
                if(err) {
                    console.log(err);
                } else if(cardset.owner.equals(req.user._id)) {
                    var response = {success: false};
                    if(req.body.white == '0') {
                        for(var i = 0, l = cardset.blackcards.length; i<l; i++) {
                            if(cardset.blackcards[i]._id.equals(req.body.id)) {
                                cardset.blackcards.splice(i, 1);
                                i = l;
                            }
                        }
                    } else {
                        for(var i = 0, l = cardset.whitecards.length; i<l; i++) {
                            if(cardset.whitecards[i]._id.equals(req.body.id)) {
                                cardset.whitecards.splice(i, 1);
                                i = l;
                            }
                        }
                    }
                    cardset.save(function (err) {
                        if (err) {
                            console.log('Failed to delete card.');
                        } else {
                            res.writeHead(200, { 'Content-Type': 'application/json' });
                            res.write(JSON.stringify({success: true}));
                            res.end();
                        }
                    });
                }
            })
        }
    });
};

exports.uploadcards = function(req, res){
    if(!req.user) return res.redirect('/login/');
    CardSet.findById(req.params.set_id, function(err, cardset) {
        if(err) {
            console.log(err);
        } else {
            User.findById(cardset.owner, function(err, owner) {
                if(err) {
                    console.log(err);
                } else if(cardset.owner.equals(req.user._id)) {
                    var form = new formidable.IncomingForm();
                    form.keepExtensions = true;
                    form.uploadDir = '/tmp';
                    form.parse(req, function(err, fields, files) {
                        if(err) {
                            console.log('error');
                        } else {
                            var fs = require('fs');
                            fs.readFile(
                                files.cardsfile.path,
                                function callback(err, data) {
                                    var lines = data.toString().split('\n');
                                    if(fields.white == '0') {
                                        for(var i = 0, l = lines.length; i<l; i++) {
                                            var blanks = (lines[i].match(/_/g)||[]).length
                                            if(blanks == 0) {
                                                blanks = 1;
                                            }
                                            cardset.blackcards.push({
                                                prompt: lines[i],
                                                blanks: blanks
                                            });
                                        }
                                    } else {
                                        for(var i = 0, l = lines.length; i<l; i++) {
                                            cardset.whitecards.push({
                                                text: lines[i]
                                            });
                                        }
                                    }
                                    cardset.save(function (err) {
                                        if (err) {
                                            console.log('Failed to add cards.', err);
                                        } else {
                                            req.flash('success', 'Added new cards to set.');
                                            res.redirect('/cards/set/'+cardset._id);
                                        }
                                    });
                                }
                            )
                        }
                    });
                }
            })
        }
    });
};