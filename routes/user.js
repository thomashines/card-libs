/*
 * User methods
 */

var db = require('../db');
var userSchemas = require('../models/user');
var User = db.conn.model("User", userSchemas.userSchema);
var mail = require('../mail');

var bcrypt = require('bcrypt');
var passport = require('passport');
var localstrat = require('passport-local').Strategy;

passport.use(new localstrat(
    function(username, password, done) {
        User.findOne({ username: username }, function (err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, 'Incorrect username.');
            }
            if (!user.validPassword(password)) {
                return done(null, false, 'Incorrect password.');
            }
            console.log(user.username+' logged in.')
            return done(null, user);
        });
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

exports.register = function(req, res){
    if(req.method == 'POST'){
        var username = req.body.username;
        var email = req.body.email;
        var password = req.body.password;
        return User.findOne({username: username}, function (err, user) {
            var warning = null;
            if (err) {
                req.flash('danger', 'Error looking up username.');
            } else if(user) {
                req.flash('danger', 'Username taken.');
            } else {
                bcrypt.genSalt(10, function(err, salt) {
                    bcrypt.hash(password, salt, function(err, hash) {
                        // Create user with this password
                        var user = new User({
                            username: username,
                            email: email,
                            password: hash,
                        });
                        user.save(function (err) {
                            if(err) {
                                console.log('Error creating user.');
                                req.flash('danger', 'Error creating user.');
                            } else {
                                var subject = 'Account Created';
                                var message =
                                    'Your Card Libs account:<br />\n'+
                                    'Username: '+username+'<br />\n'+
                                    'Email: '+email+'<br />\n'+
                                    '<a href=\'http://cardlibs.herokuapp.com/\'>Login</a>';
                                mail.send(email, subject, message)
                            }
                        });
                    });
                });
                req.flash('success', 'User account created.');
                return res.redirect('/login/');
            }
            return res.render(
                'user/register',
                {
                    user: req.user,
                    page: 'Register',
                    danger: req.flash('danger'),
                    warning: req.flash('warning'),
                    success: req.flash('success'),
                    username: username,
                    email: email,
                    password: password
                }
            );
        });
    }
    return res.render(
        'user/register',
        {
            user: req.user,
            page: 'Register',
            danger: req.flash('danger'),
            warning: req.flash('warning'),
            success: req.flash('success')
        }
    );
};

exports.passportlogin = function(req, res, next) {
    passport.authenticate(
        'local',
        function(err, user, danger) {
            if (err) {
                req.flash('danger', 'Error looking up username.');
                return res.redirect('/login/');
            } else if (!user) {
                req.flash('danger', danger);
                return res.redirect('/login/');
            } else {
                req.logIn(
                    user,
                    function(err) {
                        if (err) { return res.redirect('/login/'); }
                        req.flash('success', 'Logged in.');
                        return res.redirect('/');
                    }
                );
            }
        }
    )(req, res, next);
}

exports.login = function(req, res){
    res.render(
        'user/login',
        {
            user: req.user,
            danger: req.flash('danger'),
            warning: req.flash('warning'),
            success: req.flash('success')
        }
    );
};

exports.logout = function(req, res){
    req.logout();
    res.redirect('/login/');
};

exports.reset = function(req, res){
    if(req.method == 'POST'){
        if(req.body.username) {
            User.findOne({username: req.body.username}, function (err, user) {
                var warning = null;
                if (err) {
                    console.log('Error finding user.');
                } else if(user) {
                    require('crypto').randomBytes(4, function(ex, buf) {
                        var token = buf.toString('hex');
                        bcrypt.genSalt(10, function(err, salt) {
                            bcrypt.hash(token, salt, function(err, hash) {
                                user.password = hash;
                                user.save(function (err) {
                                    if(err) {
                                        console.log('Failed to change password.');
                                    } else {
                                        var subject = 'Password Reset';
                                        var message =
                                            'New password: '+token+'<br />\n'+
                                            'You should change this immediately.<br />\n'+
                                            '<a href=\'http://cardlibs.herokuapp.com/\'>Login</a>';
                                        mail.send(user.email, subject, message);
                                    }
                                });
                            });
                        });
                    });
                    req.flash('success', 'Your new password has been emailed to you.');
                } else {
                    req.flash('danger', 'Username not found.');
                }
                res.render(
                    'user/reset',
                    {
                        user: req.user,
                        page: 'Reset Password',
                        danger: req.flash('danger'),
                        warning: req.flash('warning'),
                        success: req.flash('success')
                    }
                );
            });
            return;
        }
    }
    res.render(
        'user/reset',
        {
            user: req.user,
            page: 'Reset Password',
            danger: req.flash('danger'),
            warning: req.flash('warning'),
            success: req.flash('success')
        }
    );
};

exports.user = function(req, res){
    if(!req.user) return res.redirect('/login/');
    var warning = null;
    if(req.method == 'POST'){
        if(req.body.password) {
            bcrypt.genSalt(10, function(err, salt) {
                bcrypt.hash(req.body.password, salt, function(err, hash) {
                    req.user.password = hash;
                    req.user.save(function (err) {
                        if (err)
                            console.log('Failed to change password.');
                    });
                });
            });
            req.flash('warning', 'Password changed.');
        }
    }
    res.render(
        'user/user',
        {
            user: req.user,
            page: 'User',
            danger: req.flash('danger'),
            warning: req.flash('warning'),
            success: req.flash('success')
        }
    );
};