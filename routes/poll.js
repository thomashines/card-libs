/*
 * Poll methods
 */

var db = require('../db');
var pollSchemas = require('../models/poll');
var Poll = db.conn.model("Poll", pollSchemas.pollSchema);

exports.index = function(req, res, next) {
    Poll.find().sort('-pubDate').limit(5).exec(
        function(err, poll) {
            if(err){
                next('Error');
            } else {
                res.render(
                    'poll/index',
                    {
                        user: req.user,
                        page: 'Poll',
                        latest_poll_list: poll
                    }
                );
            }
        }
    )
}

exports.detail = function(req, res, next) {
    poll_id = req.params.poll_id;
    Poll.findById(poll_id).exec(
        function(err, poll) {
            if (poll) {
                res.render(
                    'poll/details',
                    {
                        user: req.user,
                        page: 'Poll',
                        poll: poll
                    }
                );
            } else {
                next();
            }
        }
    )
}

exports.results = function(req, res, next) {
    poll_id = req.params.poll_id;
    Poll.findById(poll_id).exec(
        function(err, poll) {
            if (poll) {
                res.render(
                    'poll/results',
                    {
                        user: req.user,
                        page: 'Poll',
                        poll: poll
                    }
                );
            } else {
                next();
            }
        }
    )
}

exports.vote = function(req, res, next) {
    poll_id = req.params.poll_id;
    Poll.findById(poll_id).exec(
        function(err, poll) {
            if (poll) {
                var choice = req.body.choice;
                if(choice) {
                    for(var i = 0, l = poll.choices.length; i < l; i++) {
                        if(poll.choices[i]._id == choice) {
                            poll.choices[i].votes++;
                            i = l;
                        }
                    }
                    poll.save(function (err, food) { if(err) console.log(err); else console.log(food); });
                    res.writeHead(302, {
                        'Location': '/poll/'+poll._id+'/results/'
                    });
                    res.end();
                } else {
                    res.render(
                        'poll/vote',
                        {
                            user: req.user,
                            page: 'Poll',
                            poll: poll
                        }
                    );
                }
            } else {
                next();
            }
        }
    )
}